import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class Main {

    public static void main(String [] strings){
        Main main = new Main();
        Vector<Case> cases = main.fileToPeople();
        main.doMatch(cases);
    }

    public void doMatch(Vector<Case> cases){
        for (Case aCase : cases) {
            aCase.doMatch();
            aCase.printResults();
        }
    }

    //Here is the core algorithm
    protected class Case{
        private Vector<Person> manList;
        private Vector<Person> womanlist;

        public Case(Vector<Person> man, Vector<Person> woman){
            this.manList = man;
            this.womanlist = woman;
        }

        public Vector<Person> getManList() {
            return manList;
        }

        public Vector<Person> getWomanList() {
            return womanlist;
        }

        public void doMatch(){
            Person man;
            while((man=getNextManNotMatched())!=null) {
                for (int i = 0; i < man.getPreferences().length;i++){
                    Person woman = getWomanByName((man.getPreferences()[i]));
                    if(woman.getMatch()==null){
                        setPairs(man,woman);
                        break;
                    } else if(tryWomanSwapMan(woman,man)){
                        break;
                    }
                }
            }
        }

        private boolean tryWomanSwapMan(Person woman, Person man){
            boolean swapDone = false;
            char currentWomanMatchingPartnerName = woman.getMatch().getName();
            char newBachelor = man.getName();

            if(findManRank(woman, newBachelor) < findManRank(woman,woman.getMatch().getName())){
                womanMatchWithNewManAndReleaseOldMatch(woman,man);
                //System.out.println("Swapping for ..."+man.getName()+" "+woman.getName());
                swapDone = true;
            }
            return swapDone;
        }

        private void womanMatchWithNewManAndReleaseOldMatch(Person woman, Person man){
            Person oldMan = woman.getMatch();
            oldMan.setMatch(null);
            if(man.getMatch()!=null) {
                man.getMatch().setMatch(null);
            }
            woman.setMatch(man);
            man.setMatch(woman);
        }

        private int findManRank(Person woman, char man){
            int rank=-1;
            for(int index=0;index<woman.getPreferences().length;index++){
                if(woman.getPreferences()[index]==man){
                    rank = index;
                    break;
                }
            }
            return rank;
        }

        private  void setPairs(Person man, Person woman){
            man.setMatch(woman);
            woman.setMatch(man);
        }

        private void printResults(){
            //System.out.println("Result...");
            for(int i=0;i<getManList().size();i++){
                System.out.print(getManList().get(i).getName()+" ");
                if(getManList().get(i).getMatch()!=null) {
                    System.out.print(getManList().get(i).getMatch().getName());
                }
                System.out.println();
            }
            System.out.println();
        }

        private Person getWomanByName(char name){
            Person womanToReturn = null;
            for(int index = 0;index<getWomanList().size();index++){
                if(getWomanList().get(index).getName()==name){
                    womanToReturn=getWomanList().get(index);
                }
            }
            return womanToReturn;
        }

        private Person getNextManNotMatched(){
            Person manToReturn = null;
            for(int index = 0;index<getManList().size();index++){
                if(getManList().get(index).getMatch()==null){
                    manToReturn=getManList().get(index);
                }
            }
            return manToReturn;
        }
    }



    private class Person{
        char[] preferences=null;
        char name;
        Person match = null;

        public void setMatch(Person match) {
            this.match = match;
        }

        public Person(char name, char[] preferences){
            this.preferences = preferences;
            this.name=name;
        }

        public char[] getPreferences() {
            return preferences;
        }

        public char getName() {
            return name;
        }

        public Person getMatch() {
            return match;
        }
    }

    protected Vector<Case>  fileToPeople(){
        //Vector<String> lines = loadFile();
        Vector<String> lines = loadInputStream();
        Vector<Case> cases = new Vector<>();
        int ncases = Integer.parseInt(lines.get(0));
        int line = 1;
        for(int ncase = 0; ncase<ncases; ncase++){
            int nPeopleToLoad = Integer.parseInt(lines.get(line));
            line=line+2; //Skip members. Fetch it from preferences line
            cases.add(new Case(loadPersonSet(line,nPeopleToLoad,lines),loadPersonSet(line+nPeopleToLoad,nPeopleToLoad,lines)));
            line = line+nPeopleToLoad+3;
        }
        return cases;
    }

    private Vector<Person> loadPersonSet(int currentLine, int nPeopleToLoad, Vector<String> lines){
        Vector<Person> personSet = new Vector<>();
        for(int line = currentLine;line<nPeopleToLoad+currentLine;line++){
            String personLine=lines.get(line);
            char name = personLine.charAt(0);
            char[] preferences = new char[nPeopleToLoad];
            for(int nPreference=0;nPreference<nPeopleToLoad;nPreference++){
                preferences[nPreference]= personLine.charAt(2+nPreference);
            }
            personSet.add(new Person(name, preferences));
        }
        return personSet;
    }


    static String ReadLn (int maxLg)  // utility function to read from stdin
    {
        byte lin[] = new byte [maxLg];
        int lg = 0, car = -1;
        String line = "";

        try
        {
            while (lg < maxLg)
            {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin [lg++] += car;
            }
        }
        catch (IOException e)
        {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);  // eof
        return (new String (lin, 0, lg));
    }

    public Vector<String> loadInputStream() {
        String input;
        Vector<String> lines = new Vector<>();
        while ((input = Main.ReadLn (255)) != null){
            lines.add(input);
        }
        return lines;
    }

    public Vector<String> loadFile() {
        Vector<String> lines = new Vector<>();
        try {
            File file = new File(getClass().getResource("TestData").getPath());
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            fileReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }
}
